package PT2017.demo.Polinoame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

public class Main extends Application {
	
	private Stage primaryStage;
	private Pane rootLayout;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Polinoame");
		
		initRootLayout();
	}
	
	public void initRootLayout(){
		try {
			//Load root layout from fxml file
			FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("sample1.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {	
		Application.launch(args);
	}
}