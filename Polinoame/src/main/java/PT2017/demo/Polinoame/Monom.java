package PT2017.demo.Polinoame;


public class Monom implements Comparable<Object>{

	private float coefficient;
	private int power;	
	
	public Monom(float coefficient, int power){
		this.coefficient = coefficient;
		this.power = power;
	}
	
	public String toString(){
		String s = new String("");
		if(coefficient != 1 && coefficient != -1){
			if(coefficient != (int)coefficient)
				s += coefficient;
			else
				s += (int)coefficient;
		}
		
		if(coefficient == -1)
			s += "-";
		
		if(power == 0){
			s += "";
		}
		else if(power == 1){
			s += "x";
		}
		else{
			s += "x^";
			s += power;
		}
		return s;
	}
	
	public int compareTo(Object obj){
		//return ((Monom) obj).getPower() - power; 
		return power - ((Monom) obj).getPower();
	}
	
	public int getPower() {
		return power;
	}
	
	public void setPower(int power) {
		this.power = power;
	}
	
	public float getCoefficient() {
		return coefficient;
	}
	
	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}
}	